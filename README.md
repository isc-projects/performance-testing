# performance-testing for Kea dhcp server

A python performance testing framework.
This is a Linux testing framework using systemd for managing services.
It runs performance tests for memfile, mysql, postgresql, cassandra, v4, v6 na, v6 pd, single-threading and multi-threading.

The testing framework uses hardcoded path values:
/opt/kea-work -> kea and perfdhcp installation (use --prefix when configuring and compiling kea)

Tests are also hardcoded:
50 iterations for 10 seconds for each test

Cassandra tests are disabled for now

The dhcp traffic is generated using perfdhcp

Output:
- result file for each test
- pcap file (empty by default as this is disabled in the tests)
- statistics for each type of test
- graphical comparison between tests as png image
- exported graphical data as pickle file, printable by using plot_data.py

With minimul programming/coding skills, desired features can be enabled/disabled and tests can be tweaked (increase rate, enable/disable, etc.)
