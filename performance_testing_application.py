import inspect
import logging
import math
import matplotlib.pyplot
import os
import pickle
import shutil
import time

import common.test_application


class PerformanceTestingApplication(common.test_application.TestApplication):
    def __init__(self, path):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        super(PerformanceTestingApplication, self).__init__(path, 'performance-testing-application')
        self.testManager_ = None
        return

    def setup(self):
        self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name)
        super(PerformanceTestingApplication, self).setup()
        import common.test_manager
        self.testManager_ = common.test_manager.TestManager(self)
        return

    def load(self, config):
        self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name)
        result = super(PerformanceTestingApplication, self).load(config)
        return result

    def apply(self):
        self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name)
        super(PerformanceTestingApplication, self).apply()
        return

    def run(self):
        self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name)
        self.setup()
        super(PerformanceTestingApplication, self).run()
        return

    def work(self):
        # logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        try:
            self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name, 'running tests')
            self.serviceManager_.installServices()
            self.setupManager_.setUp()
            test_list = ['st',
                         'mt'
                        ]
            db_type_list = ['memfile',
                            # 'cql'
                            'mysql',
                            'postgresql'
                           ]
            test_type_list = ['v4',
                              'v6-na',
                              'v6-pd'
                             ]
            thread_count_list = [2, 4, 6, 8, 12, 16, 18, 24, 36]
            queue_size_list = [1, 2, 4, 8, 16]
            rate = 10000
            period = 10
            try_count = 50
            thread_count = 6
            queue_size = thread_count
            x = range(0, try_count)
            test_result = {}
            test_result['ramp-up'] = {}
            figure = None
            for performance_type in test_list:
                matplotlib.pyplot.title('performance testing results ' + performance_type)
                matplotlib.pyplot.xlabel('iteration')
                matplotlib.pyplot.ylabel('leases/s')
                test_result['ramp-up'][performance_type] = {}
                for db_type in db_type_list:
                    if db_type == 'memfile':
                        if performance_type == 'st':
                            rate = 20000
                        if performance_type == 'mt':
                            rate = 40000
                    else:
                        if performance_type == 'st':
                            rate = 10000
                        if performance_type == 'mt':
                            rate = 20000
                    test_result['ramp-up'][performance_type][db_type] = {}
                    self.testManager_.setUp(performance_type, db_type, thread_count, queue_size)
                    for test_type in test_type_list:
                        test_result['ramp-up'][performance_type][db_type][test_type] = {}
                        label = ''
                        if performance_type == 'mt':
                            test_result['ramp-up'][performance_type][db_type][test_type]['thread'] = {}
                            test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min-id'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max-id'] = -1
                            for count in thread_count_list:
                                test_size = 4 * count
                                self.testManager_.updateFiles(performance_type, db_type, count, test_size)
                                z, temp_result = self.update(try_count, performance_type, test_type, db_type, rate, period, '-N-' + str(count))
                                test_result['ramp-up'][performance_type][db_type][test_type]['thread'][str(count)] = temp_result
                                if test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min'] == -1:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min'] = temp_result['min']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min-id'] = count
                                if test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max'] == -1:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max'] = temp_result['max']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max-id'] = count
                                if temp_result['max'] > test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max']:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max'] = temp_result['max']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max-id'] = count
                                if temp_result['min'] < test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min']:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min'] = temp_result['min']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['thread']['min-id'] = count
                            test_result['ramp-up'][performance_type][db_type][test_type]['queue'] = {}
                            test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min-id'] = -1
                            test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max-id'] = -1
                            for size in queue_size_list:
                                count = test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max-id']
                                test_size = size * count
                                self.testManager_.updateFiles(performance_type, db_type, count, test_size)
                                z, temp_result = self.update(try_count, performance_type, test_type, db_type, rate, period, '-Q-' + str(size))
                                test_result['ramp-up'][performance_type][db_type][test_type]['queue'][str(size)] = temp_result
                                if test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min'] == -1:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min'] = temp_result['min']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min-id'] = size
                                if test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max'] == -1:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max'] = temp_result['max']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max-id'] = size
                                if temp_result['max'] > test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max']:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max'] = temp_result['max']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max-id'] = size
                                if temp_result['min'] < test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min']:
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min'] = temp_result['min']
                                    test_result['ramp-up'][performance_type][db_type][test_type]['queue']['min-id'] = size
                            thread_count = test_result['ramp-up'][performance_type][db_type][test_type]['thread']['max-id']
                            queue_size = test_result['ramp-up'][performance_type][db_type][test_type]['queue']['max-id']
                            queue_size = queue_size * thread_count
                            label = '-N-' + str(thread_count) + '-Q-' + str(queue_size)
                            self.testManager_.updateFiles(performance_type, db_type, thread_count, queue_size)
                        y, test_result['ramp-up'][performance_type][db_type][test_type]['data'] = self.update(try_count, performance_type, test_type, db_type, rate, period, '')
                        figure = matplotlib.pyplot.plot(x, y, label=performance_type + '-' + db_type + '-' + test_type + label)
                    self.testManager_.tearDown(db_type)
                matplotlib.pyplot.legend()
                matplotlib.pyplot.savefig(self.cwd_ + '/results/' + performance_type + '.png')
                pickle.dump(figure, open(self.cwd_ + '/results/' + performance_type + '.pickle', 'wb'))
                matplotlib.pyplot.close()
            self.setupManager_.tearDown()
            self.serviceManager_.uninstallServices()
            for db_type in db_type_list:
                matplotlib.pyplot.title('performance testing results ' + db_type)
                matplotlib.pyplot.xlabel('iteration')
                matplotlib.pyplot.ylabel('leases/s')
                for performance_type in test_list:
                    for test_type in test_type_list:
                        y = []
                        for iteration in range(0, try_count):
                            value = test_result['ramp-up'][performance_type][db_type][test_type]['data'][str(iteration)]
                            y.append((value['ra-rr']['recv'] - value['ra-rr']['rej']) / period)
                        figure = matplotlib.pyplot.plot(x, y, label=performance_type + '-' + db_type + '-' + test_type)
                matplotlib.pyplot.legend()
                matplotlib.pyplot.savefig(self.cwd_ + '/results/' + performance_type + '-' + db_type + '.png')
                pickle.dump(figure, open(self.cwd_ + '/results/' + performance_type + '-' + db_type + '.pickle', 'wb'))
                matplotlib.pyplot.close()
            x = [str(i) for i in thread_count_list]
            for db_type in db_type_list:
                matplotlib.pyplot.title('performance testing results ' + db_type)
                matplotlib.pyplot.xlabel('threads')
                matplotlib.pyplot.ylabel('leases/s')
                for test_type in test_type_list:
                    y = []
                    for count in thread_count_list:
                        value = test_result['ramp-up'][performance_type][db_type][test_type]['thread'][str(count)]
                        y.append(value['mean'])
                    figure = matplotlib.pyplot.plot(x, y, label=performance_type + '-' + db_type + '-' + test_type + '-thread-count')
                matplotlib.pyplot.legend()
                matplotlib.pyplot.savefig(self.cwd_ + '/results/' + performance_type + '-' + db_type + '-thread-count.png')
                pickle.dump(figure, open(self.cwd_ + '/results/' + performance_type + '-' + db_type + '-thread-count.pickle', 'wb'))
                matplotlib.pyplot.close()
            x = [str(i) for i in queue_size_list]
            for db_type in db_type_list:
                matplotlib.pyplot.title('performance testing results ' + db_type)
                matplotlib.pyplot.xlabel('queue')
                matplotlib.pyplot.ylabel('leases/s')
                for test_type in test_type_list:
                    y = []
                    for size in queue_size_list:
                        value = test_result['ramp-up'][performance_type][db_type][test_type]['queue'][str(size)]
                        y.append(value['mean'])
                    figure = matplotlib.pyplot.plot(x, y, label=performance_type + '-' + db_type + '-' + test_type + '-queue-size')
                matplotlib.pyplot.legend()
                matplotlib.pyplot.savefig(self.cwd_ + '/results/' + performance_type + '-' + db_type + '-queue-size.png')
                pickle.dump(figure, open(self.cwd_ + '/results/' + performance_type + '-' + db_type + '-queue-size.pickle', 'wb'))
                matplotlib.pyplot.close()
        except Exception as e:
            self.logCritical(__name__ + '.' + inspect.currentframe().f_code.co_name, 'can not compute iteration error ' + str(e))
            self.stop()
            return False
        self.stop()
        return True

    def update(self, try_count, performance_type, test_type, db_type, rate, period, label):
        self.logDebug(__name__ + '.' + inspect.currentframe().f_code.co_name)
        result = {}
        result['min'] = -1
        result['max'] = -1
        result['mean'] = 0
        result['deviation'] = 0
        y = []
        for iteration in range(0, try_count):
            value = self.testManager_.runTest(performance_type, test_type, db_type, rate, period, 'ramp-up-' + str(iteration) + label)
            result[str(iteration)] = value
            test_value = (value['ra-rr']['recv'] - value['ra-rr']['rej']) / period
            y.append(test_value)
            if result['min'] == -1:
                result['min'] = test_value
            if result['max'] == -1:
                result['max'] = test_value
            if test_value > result['max']:
                result['max'] = test_value
            if test_value < result['min']:
                result['min'] = test_value
            result['mean'] = test_value + result['mean']
        result['mean'] = (result['mean'] * 1.0) / try_count
        deviation = 0
        mean = result['mean']
        for y_value in y:
            deviation = deviation + (y_value - mean) * (y_value - mean)
        deviation = (deviation * 1.0) / try_count
        deviation = math.sqrt(deviation)
        result['deviation'] = deviation
        with open(self.cwd_ + '/results/' + performance_type + '-' + db_type + '-' + test_type + label + '-stats.txt', 'wt') as statsFile:
            statsFile.write('min: ' + str(result['min']) + '\n')
            statsFile.write('max: ' + str(result['max']) + '\n')
            statsFile.write('mean: ' + str(result['mean']) + '\n')
            statsFile.write('deviation: ' + str(result['deviation']) + '\n')
        return y, result

    def handleSignals(self):
        # logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        for signal in self.signals_:
            self.signals_.remove(signal)
            raise Exception('signal ' + str(signal) + ' not handled')
        return
