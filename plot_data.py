#!/usr/bin/python
import matplotlib.pyplot
import pickle
import sys


data = pickle.load(open(sys.argv[1], 'rb'))
matplotlib.pyplot.title(sys.argv[1])
matplotlib.pyplot.ylabel('leases/s')
matplotlib.pyplot.xlabel('iteration')
x1,x2,y1,y2 = matplotlib.pyplot.axis()
matplotlib.pyplot.axis((x1,x2,0,6000))
matplotlib.pyplot.show()
