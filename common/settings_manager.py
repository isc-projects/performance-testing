import inspect
import json
import os
import shutil

import common.manager as manager


class SettingsManager(manager.Manager):
    def __init__(self, application):
        super(SettingsManager, self).__init__(__name__, application)
        self.logDebug(inspect.currentframe().f_code.co_name)
        self.config_ = {}
        self.write(self.read(self.application_.settingsFile_), self.application_.settingsFile_)
        self.load(self.read(self.application_.settingsFile_))
        self.apply()
        return

    def read(self, path):
        self.logDebug(inspect.currentframe().f_code.co_name)
        config = {}
        try:
            with open(path, 'r') as settingsFile:
                config = json.loads(settingsFile.read())
                if type(config) != type({}):
                    config = {}
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not read settings file ' + path + ' error ' + str(e))
        return config

    def write(self, config, path):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            with open(path, 'w') as settingsFile:
                json.dump(config, settingsFile, indent=4, sort_keys=True)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not write settings file ' + path + ' error ' + str(e))
        return

    def load(self, config):
        self.logDebug(inspect.currentframe().f_code.co_name)
        data = False
        try:
            data = self.application_.load(config)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not load application settings error ' + str(e))
        if data:
            self.config_ = config
        return

    def apply(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            self.application_.apply()
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not apply application settings error ' + str(e))
        return
