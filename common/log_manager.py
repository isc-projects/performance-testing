import inspect
import logging


class LogManager(object):
    def __init__(self, application):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.application_ = application
        self.logger_ = None
        self.logLevel_ = None
        self.setup()
        return

    def setup(self):
        self.apply()
        return

    def apply(self):
        self.logLevel_ = self.application_.logger_
        self.logger_ = logging.getLogger(self.logLevel_)
        return

    def logDebug(self, message):
        try:
            self.logger_.debug(message)
        except Exception as e:
            self.logger_.critical('error logging message: ' + str(e))
            self.logger_.debug(message)
        return

    def logInfo(self, message):
        try:
            self.logger_.info(message)
        except Exception as e:
            self.logger_.critical('error logging message: ' + str(e))
            self.logger_.info(message)
        return

    def logWarning(self, message):
        try:
            self.logger_.warning(message)
        except Exception as e:
            self.logger_.critical('error logging message: ' + str(e))
            self.logger_.warning(message)
        return

    def logError(self, message):
        try:
            self.logger_.error(message)
        except Exception as e:
            self.logger_.critical('error logging message: ' + str(e))
            self.logger_.error(message)
        return

    def logCritical(self, message):
        try:
            self.logger_.critical(message)
        except Exception as e:
            self.logger_.critical('error logging message: ' + str(e))
            self.logger_.critical(message)
        return
