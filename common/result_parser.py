import inspect
import logging


class ResultParser(object):
    def __init__(self, application):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.application_ = application
        self.stage_ = None
        return

    def parse(self, lines, save=False, file_name=None):
        self.application_.logDebug(inspect.currentframe().f_code.co_name)
        result = {'do-sa': {'send': 0, 'recv': 0, 'drop': 0, 'orph': 0, 'rej': 0, 'non-u': 0},
                  'ra-rr': {'send': 0, 'recv': 0, 'drop': 0, 'orph': 0, 'rej': 0, 'non-u': 0}
                 }
        if not save:
            file_name = '/dev/null'
        try:
            with open(file_name, 'w') as result_file:
                for line in lines.splitlines():
                    self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing ' + line)
                    if save:
                        result_file.write(line + '\n')
                    data = line.lower()
                    if data.find('discover-offer') != -1:
                        self.stage_ = 'do-sa'
                        continue
                    if data.find('solicit-advertise') != -1:
                        self.stage_ = 'do-sa'
                        continue
                    if data.find('request-ack') != -1:
                        self.stage_ = 'ra-rr'
                        continue
                    if data.find('request-reply') != -1:
                        self.stage_ = 'ra-rr'
                        continue
                    if data.find('sent packets: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['send'] = value
                        continue
                    if data.find('received packets: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['recv'] = value
                        continue
                    if data.find('drops: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['drop'] = value
                        continue
                    if data.find('orphans: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['orph'] = value
                        continue
                    if data.find('rejected leases: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['rej'] = value
                        continue
                    if data.find('non unique addresses: ') != -1:
                        data = data.split(': ')
                        data = data[1]
                        value = 0
                        try:
                            value = int(data)
                        except Exception as e:
                            self.application_.logDebug(inspect.currentframe().f_code.co_name, 'parsing line ' + line + ' error ' + str(e))
                        result[self.stage_]['non-u'] = value
                        continue
        except Exception as e:
            self.application_.logError(inspect.currentframe().f_code.co_name, 'parse error ' + str(e))
        return result
