import inspect
import logging


class Application(object):
    def __init__(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.done_ = True
        self.signals_ = []
        return

    def run(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        while not self.done_:
            self.handleSignals()
            self.doWork()
        return

    def doWork(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        return

    def start(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.done_ = False
        return

    def stop(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.done_ = True
        return

    def registerSignal(self, signal):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.signals_.append(signal)
        return

    def handleSignals(self):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        return

    def logDebug(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        logging.debug(logMessage)
        return

    def logInfo(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        logging.info(logMessage)
        return

    def logWarning(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        logging.warning(logMessage)
        return

    def logError(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        logging.error(logMessage)
        return

    def logCritical(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        logging.critical(logMessage)
        return
