import inspect
import os
import shutil

import common.manager as manager


class SetupManager(manager.Manager):
    def __init__(self, application):
        super(SetupManager, self).__init__(__name__, application)
        self.logDebug(inspect.currentframe().f_code.co_name)
        return

    def setUp(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            command_list = ['ip netns add sns',
                            'ip netns add cns',
                            'ip link add vethsns type veth peer name vethcns',
                            'ip link add vpgs type veth peer name vpgcns',
                            'ip link set vethsns netns sns',
                            'ip link set vpgcns netns sns',
                            'ip netns exec sns ip link set dev vethsns up',
                            'ip netns exec sns ip link set dev vpgcns up',
                            'ip link set dev vpgs up',
                            'ip addr add 192.169.0.1/24 dev vpgs',
                            'ip netns exec sns ip addr add 192.169.0.2/24 dev vpgcns',
                            'ip netns exec sns ip addr add 192.168.0.1/24 dev vethsns',
                            'ip netns exec sns ip -6 addr add 2001:db8::1/64 dev vethsns',
                            'ip link set vethcns netns cns',
                            'ip netns exec cns ip link set dev vethcns up',
                            'ip netns exec cns ip addr add 192.168.0.2/24 dev vethcns',
                            'ip netns exec cns ip -6 addr add 2001:db8::2/64 dev vethcns'
                           ]
            for command in command_list:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                if code:
                    self.logError(__name__ + '.' + inspect.currentframe().f_code.co_name, 'command: ' + command + ' exited with error code: ' + str(code) + ' output: ' + result + ' error: ' + error)
            self.application_.serviceManager_.stop('kea-dhcp4-st')
            self.application_.serviceManager_.stop('kea-dhcp6-st')
            self.application_.serviceManager_.stop('kea-dhcp4-mt')
            self.application_.serviceManager_.stop('kea-dhcp6-mt')
            # self.application_.serviceManager_.restart('cassandra')
            self.application_.serviceManager_.restart('mysql')
            self.application_.serviceManager_.restart('postgresql')
            path = self.application_.cwd_ + '/results/'
            if os.path.exists(path):
                shutil.rmtree(path)
            self.application_.utilsManager_.mkdir(path)
            self.logInfo(inspect.currentframe().f_code.co_name, 'set up completed')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'set up error ' + str(e))
        return

    def tearDown(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            pid = os.getpid()
            command_list = ['ip netns exec sns ip link set vethsns netns ' + str(pid),
                            'ip netns exec sns ip link set vpgcns netns ' + str(pid),
                            'ip netns exec cns ip link set vethcns netns ' + str(pid),
                            'ip link del vethsns type veth peer name vethcns',
                            'ip link del vpgs type veth peer name vpgcns',
                            'ip netns del sns',
                            'ip netns del cns'
                           ]
            for command in command_list:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                if code:
                    self.logError(__name__ + '.' + inspect.currentframe().f_code.co_name, 'command: ' + command + ' exited with error code: ' + str(code) + ' output: ' + result + ' error: ' + error)
            self.application_.serviceManager_.stop('kea-dhcp4-st')
            self.application_.serviceManager_.stop('kea-dhcp6-st')
            self.application_.serviceManager_.stop('kea-dhcp4-mt')
            self.application_.serviceManager_.stop('kea-dhcp6-mt')
            # self.application_.serviceManager_.restart('cassandra')
            self.application_.serviceManager_.restart('mysql')
            self.application_.serviceManager_.restart('postgresql')
            self.logInfo(inspect.currentframe().f_code.co_name, 'tear down completed')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'tear down error ' + str(e))
        return
