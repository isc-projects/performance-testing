import hashlib
import inspect
import json
import os
import shlex
import subprocess
import tarfile

import common.manager as manager


class UtilsManager(manager.Manager):
    def __init__(self, application):
        super(UtilsManager, self).__init__(__name__, application)
        self.logDebug(inspect.currentframe().f_code.co_name)
        return

    def runCommand(self, command):
        self.logDebug(inspect.currentframe().f_code.co_name)
        code = 0
        try:
            with open('/dev/null', 'w') as nullFile:
                code = subprocess.call(shlex.split(command), stdout=nullFile, stderr=nullFile)
            if not code:
                self.logDebug(inspect.currentframe().f_code.co_name, 'command ' + command + ' exited with exit code 0')
            else:
                self.logDebug(inspect.currentframe().f_code.co_name, 'command ' + command + ' exited with exit code ' + str(code))
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'could not run command ' + command + ' error ' + str(e))
        return code

    def runCommandWithResponse(self, command):
        self.logDebug(inspect.currentframe().f_code.co_name)
        result = ''
        code = 0
        error = ''
        try:
            process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
            (output, error) = process.communicate()
            code = process.wait()
            output = output.decode()
            error = error.decode()
            result = output
            if not code:
                self.logDebug(inspect.currentframe().f_code.co_name, 'command ' + command + ' exited with exit code 0 output [\n' + result + '\n] error [\n' + error + '\n]')
            else:
                self.logDebug(inspect.currentframe().f_code.co_name, 'command ' + command + ' exited with exit code ' + str(code) + ' output [\n' + result + '\n] error [\n' + error + '\n]')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'could not run command ' + command + ' error ' + str(e))
        return result, error, code

    def md5sum(self, path, blocksize=65536):
        self.logDebug(inspect.currentframe().f_code.co_name)
        result = ''
        try:
            hashData = hashlib.md5()
            with open(path, 'rb') as md5File:
                block = md5File.read(blocksize)
                while block:
                    hashData.update(block)
                    block = md5File.read(blocksize)
            result = hashData.hexdigest()
            self.logDebug(inspect.currentframe().f_code.co_name, 'md5sum for file ' + path + ' is ' + result)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'could not compute md5sum for file ' + path + ' error ' + str(e))
        return result

    def extract(self, source, destination):
        self.logDebug(inspect.currentframe().f_code.co_name)
        code = 0
        try:
            with tarfile.open(source) as tarFile:
                tarFile.extractall(destination)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'could not extract tar archive file ' + source + ' error ' + str(e))
            code = -1
        return code

    def mkdir(self, path):
        self.logDebug(inspect.currentframe().f_code.co_name)
        directory = os.path.dirname(path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return
