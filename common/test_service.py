import inspect
import os

import common.service as service


class TestService(service.Service):
    def __init__(self, name, manager, path=None, tag=None):
        super(TestService, self).__init__(name, manager)
        self.manager_.logDebug(inspect.currentframe().f_code.co_name)
        self.scriptPath_ = ''
        self.serviceLogTag_ = ''
        if path:
            self.scriptPath_ = path
        if tag:
            self.serviceLogTag_ = tag
        return

    def install(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        try:
            os.symlink(self.scriptPath_ + self.name_ + '.service', '/etc/systemd/system/' + self.name_ + '.service')
            self.manager_.logInfo(inspect.currentframe().f_code.co_name, 'installed service ' + self.name_ + '.service file')
        except Exception as e:
            self.manager_.logError(inspect.currentframe().f_code.co_name, 'can not install service ' + self.name_ + '.service file error ' + str(e))
        return

    def uninstall(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        try:
            os.remove('/etc/systemd/system/' + self.name_ + '.service')
            self.manager_.logInfo(inspect.currentframe().f_code.co_name, 'uninstalled service ' + self.name_ + '.service file')
        except Exception as e:
            self.manager_.logError(inspect.currentframe().f_code.co_name, 'can not uninstall service ' + self.name_ + '.service file error ' + str(e))
        return
