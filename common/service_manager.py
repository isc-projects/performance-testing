import inspect

import common.test_service as test_service
import common.manager as manager
import common.service as service


class ServiceManager(manager.Manager):
    def __init__(self, application):
        super(ServiceManager, self).__init__(__name__, application)
        self.logDebug(inspect.currentframe().f_code.co_name)
        self.services_ = self.register()
        return

    def register(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        services = {}
        applicationPath = self.application_.cwd_ + '/resources/services/'
        services_list = ['tcpdump-testing',
                         'kea-dhcp4-st',
                         'kea-dhcp6-st',
                         'kea-dhcp4-mt',
                         'kea-dhcp6-mt'
                        ]
        for name in services_list:
            services[name] = test_service.TestService(name, self, applicationPath, name)
        services['mysql'] = service.Service('mysql', self)
        services['postgresql'] = service.Service('postgresql', self)
        services['cassandra'] = service.Service('cassandra', self)
        return services

    def install(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not install service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].install()
            self.application_.utilsManager_.runCommand('systemctl daemon-reload')
            self.logInfo(inspect.currentframe().f_code.co_name, 'installed service ' + service + '.service file')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not install service ' + service + '.service file error ' + str(e))
        return

    def uninstall(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not uninstall service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].uninstall()
            self.application_.utilsManager_.runCommand('systemctl daemon-reload')
            self.logInfo(inspect.currentframe().f_code.co_name, 'uninstalled service ' + service + '.service file')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not uninstall service ' + service + '.service file error ' + str(e))
        return

    def enable(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not enable service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].enable()
            self.application_.utilsManager_.runCommand('systemctl enable ' + service + '.service')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not enable service ' + service + '.service file error ' + str(e))
        return

    def disable(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not disable service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].disable()
            self.application_.utilsManager_.runCommand('systemctl disable ' + service + '.service')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not disable service ' + service + '.service file error ' + str(e))
        return

    def start(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not start service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].start()
            self.application_.utilsManager_.runCommand('systemctl start ' + service + '.service')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not start service ' + service + '.service file error ' + str(e))
        return

    def stop(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not stop service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].stop()
            self.application_.utilsManager_.runCommand('systemctl stop ' + service + '.service')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not stop service ' + service + '.service file error ' + str(e))
        return

    def restart(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not restart service ' + service + '.service file error service not found')
            return
        try:
            self.services_[service].restart()
            self.application_.utilsManager_.runCommand('systemctl restart ' + service + '.service')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not restart service ' + service + '.service file error ' + str(e))
        return

    def status(self, service):
        self.logDebug(inspect.currentframe().f_code.co_name)
        if not service in self.services_:
            self.logError(inspect.currentframe().f_code.co_name, 'can not get service ' + service + '.service file status error service not found')
            return
        status = ''
        foundStatus = ''
        try:
            self.services_[service].status()
            status = self.application_.utilsManager_.runCommandWithResponse('systemctl status ' + service + '.service')[0]
            for line in status.splitlines():
                line = line.replace('\t', ' ')
                line = line.lower()
                if line.find('active: ') != -1:
                    line = line.split(': ')
                    line = line[1]
                    line = line.split()
                    status = line[0]
                    foundStatus = 'yes'
                    break
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not get service ' + service + '.service file status error ' + str(e))
        if not foundStatus:
            self.logError(inspect.currentframe().f_code.co_name, 'failed to get service ' + service + '.service file status error unknown status using default value ' + status)
        return {'status': status}

    def installServices(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        for name, service in sorted(self.services_.items()):
            self.logDebug(inspect.currentframe().f_code.co_name, 'installing service ' + name)
            self.install(name)
        return

    def uninstallServices(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        for name, service in sorted(self.services_.items()):
            self.logDebug(inspect.currentframe().f_code.co_name, 'uninstalling service ' + name)
            self.uninstall(name)
        return
