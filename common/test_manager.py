import inspect
import json
import os
import shutil
import time

import common.manager as manager
import common.result_parser as result_parser


class TestManager(manager.Manager):
    def __init__(self, application):
        super(TestManager, self).__init__(__name__, application)
        self.logDebug(inspect.currentframe().f_code.co_name)
        return

    def setUp(self, performance_type, db_type, thread_count, queue_size):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            self.application_.serviceManager_.stop('kea-dhcp4-st')
            self.application_.serviceManager_.stop('kea-dhcp6-st')
            self.application_.serviceManager_.stop('kea-dhcp4-mt')
            self.application_.serviceManager_.stop('kea-dhcp6-mt')
            self.application_.serviceManager_.stop('cassandra')
            self.application_.serviceManager_.stop('mysql')
            self.application_.serviceManager_.stop('postgresql')
            if (db_type == 'cql'):
                self.application_.serviceManager_.restart('cassandra')
                time.sleep(30)
            if (db_type == 'mysql'):
                self.application_.serviceManager_.restart('mysql')
                time.sleep(3)
            if (db_type == 'postgresql'):
                self.application_.serviceManager_.restart('postgresql')
                time.sleep(3)
            self.updateFiles(performance_type, db_type, thread_count, queue_size)
            try:
                config_file = '/opt/kea-work/etc/kea/kea-dhcp4-testing.conf'
                os.remove(config_file)
                self.logInfo(inspect.currentframe().f_code.co_name, 'removed ' + config_file + ' file')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'can not remove ' + config_file + ' file error ' + str(e))
            os.symlink(self.application_.cwd_ + '/resources/configs/kea-dhcp4-' + performance_type + '-' + db_type + '.conf' , config_file)
            try:
                config_file = '/opt/kea-work/etc/kea/kea-dhcp6-testing.conf'
                os.remove(config_file)
                self.logInfo(inspect.currentframe().f_code.co_name, 'removed ' + config_file + ' file')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'can not remove ' + config_file + ' file error ' + str(e))
            os.symlink(self.application_.cwd_ + '/resources/configs/kea-dhcp6-' + performance_type + '-' + db_type + '.conf' , config_file)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'set up error ' + str(e))
        return

    def tearDown(self, db_type):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            self.logInfo(inspect.currentframe().f_code.co_name, 'tear down completed')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'tear down error ' + str(e))
        return

    def updateFiles(self, performance_type, db_type, thread_count, queue_size):
        self.logDebug(inspect.currentframe().f_code.co_name)
        config_name = self.application_.cwd_ + '/resources/configs/kea-dhcp4-' + performance_type + '-' + db_type + '.conf'
        template = config_name + '.temp'
        config = {}
        with open(template, 'r') as config_file:
            config = json.loads(config_file.read())
        if performance_type == 'mt':
            config['Dhcp4']['multi-threading']['thread-pool-size'] = thread_count
            config['Dhcp4']['multi-threading']['packet-queue-size'] = queue_size
        with open(config_name, 'w') as config_file:
            json.dump(config, config_file)
        config_name = self.application_.cwd_ + '/resources/configs/kea-dhcp6-' + performance_type + '-' + db_type + '.conf'
        template = config_name + '.temp'
        config = {}
        with open(template, 'r') as config_file:
            config = json.loads(config_file.read())
        if performance_type == 'mt':
            config['Dhcp6']['multi-threading']['thread-pool-size'] = thread_count
            config['Dhcp6']['multi-threading']['packet-queue-size'] = queue_size
        with open(config_name, 'w') as config_file:
            json.dump(config, config_file)
        return

    def runTest(self, performance_type, test_type, db_type, rate, period, label):
        self.logDebug(inspect.currentframe().f_code.co_name)
        self.application_.serviceManager_.stop('kea-dhcp4-st')
        self.application_.serviceManager_.stop('kea-dhcp6-st')
        self.application_.serviceManager_.stop('kea-dhcp4-mt')
        self.application_.serviceManager_.stop('kea-dhcp6-mt')
        if db_type == 'memfile':
            try:
                lease_file = '/opt/kea-work/var/lib/kea/kea-leases4.csv'
                os.remove(lease_file)
                self.logDebug(inspect.currentframe().f_code.co_name, 'removed ' + lease_file + ' file')
            except Exception as e:
                self.logDebug(inspect.currentframe().f_code.co_name, 'can not remove ' + lease_file + ' file error ' + str(e))
            try:
                lease_file = '/opt/kea-work/var/lib/kea/kea-leases6.csv'
                os.remove(lease_file)
                self.logDebug(inspect.currentframe().f_code.co_name, 'removed ' + lease_file + ' file')
            except Exception as e:
                self.logDebug(inspect.currentframe().f_code.co_name, 'can not remove ' + lease_file + ' file error ' + str(e))
        if db_type == 'cql':
            command = '/opt/kea-work/sbin/kea-admin db-version cql -u keatest -p keatest -n keatest'
            version = 0
            try:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                version = result.splitlines()[0]
                self.logDebug(inspect.currentframe().f_code.co_name, 'retrieved ' + db_type + ' database schema version ' + version)
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to retrieve ' + db_type + ' database schama version error ' + str(e))
            command = 'sh /opt/kea-work/share/kea/scripts/cql/wipe_data.sh ' + version + ' -u keatest -p keatest -k keatest --request-timeout=10000'
            try:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                self.logDebug(inspect.currentframe().f_code.co_name, 'clear all data from ' + db_type + ' database')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to clear all data from ' + db_type + ' database error ' + str(e))
        if db_type == 'mysql':
            command_list = ['mysql --user=keatest --password=keatest --execute="DROP DATABASE keatest;"',
                            'mysql --user=keatest --password=keatest --execute="CREATE DATABASE keatest;"'
                           ]
            try:
                for command in command_list:
                    result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                self.logDebug(inspect.currentframe().f_code.co_name, 'created ' + db_type + ' database')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to create ' + db_type + ' database error ' + str(e))
            command = '/opt/kea-work/sbin/kea-admin db-init mysql -u keatest -p keatest'
            try:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                self.logDebug(inspect.currentframe().f_code.co_name, 'set up ' + db_type + ' database')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to set up ' + db_type + ' database error ' + str(e))
        if db_type == 'postgresql':
            os.environ['PGPASSWORD'] = 'root'
            command_list = ['sudo -u postgres psql --user=postgres --no-password --command="REVOKE ALL ON DATABASE keatest FROM keatest;"',
                            'sudo -u postgres psql --user=postgres --no-password --command="DROP DATABASE keatest;"',
                            'sudo -u postgres psql --user=postgres --no-password --command="CREATE DATABASE keatest;"',
                            'sudo -u postgres psql --user=postgres --no-password --command="GRANT ALL PRIVILEGES ON DATABASE keatest to keatest;"'
                           ]
            try:
                for command in command_list:
                    result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                self.logDebug(inspect.currentframe().f_code.co_name, 'created ' + db_type + ' database')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to create ' + db_type + ' database error ' + str(e))
            os.environ['PGPASSWORD'] = 'keatest'
            command = '/opt/kea-work/sbin/kea-admin db-init pgsql -u keatest -n keatest'
            try:
                result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
                self.logDebug(inspect.currentframe().f_code.co_name, 'set up ' + db_type + ' database')
            except Exception as e:
                self.logError(inspect.currentframe().f_code.co_name, 'failed to set up ' + db_type + ' database error ' + str(e))
        try:
            capture_file = '/opt/kea-work/etc/kea/capture.pcap'
            os.remove(capture_file)
            self.logInfo(inspect.currentframe().f_code.co_name, 'removed ' + capture_file + ' file')
        except Exception as e:
            self.logDebug(inspect.currentframe().f_code.co_name, 'can not remove ' + capture_file + ' file error ' + str(e))
        os.mknod(self.application_.cwd_ + '/results/capture-' + performance_type + '-' + test_type + '-' + db_type + '-' + label + '.pcap')
        os.symlink(self.application_.cwd_ + '/results/capture-' + performance_type + '-' + test_type + '-' + db_type + '-' + label + '.pcap' , capture_file)
        command = ''
        result_file = ''
        test_result = None
        requests = period * rate
        if (test_type == 'v4'):
            self.application_.serviceManager_.restart('kea-dhcp4-' + performance_type)
            time.sleep(3)
            command = 'ip netns exec cns /opt/kea-work/sbin/perfdhcp -p ' + str(period) + ' -4 -u -r ' + str(rate) + ' -n ' + str(requests) + ' -R ' + str(requests) + ' -L 67 -M 67 192.168.0.1'
        if (test_type == 'v6-na'):
            self.application_.serviceManager_.restart('kea-dhcp6-' + performance_type)
            time.sleep(3)
            command = 'ip netns exec cns /opt/kea-work/sbin/perfdhcp -p ' + str(period) + ' -6 -u -r ' + str(rate) + ' -n ' + str(requests) + ' -R ' + str(requests) + ' -e address-only -l vethcns'
        if (test_type == 'v6-pd'):
            self.application_.serviceManager_.restart('kea-dhcp6-' + performance_type)
            time.sleep(3)
            command = 'ip netns exec cns /opt/kea-work/sbin/perfdhcp -p ' + str(period) + ' -6 -u -r ' + str(rate) + ' -n ' + str(requests) + ' -R ' + str(requests) + ' -e prefix-only -l vethcns'
        result_file = '/results/result-' + performance_type + '-' + test_type + '-' + db_type
        try:
            self.logInfo(inspect.currentframe().f_code.co_name, 'run ' + performance_type + ' ' + test_type + ' ' + db_type + ' rate ' + str(rate) + ' period ' + str(period) + ' test started')
            # self.application_.serviceManager_.start('tcpdump-testing')
            result, error, code = self.application_.utilsManager_.runCommandWithResponse(command)
            parser = result_parser.ResultParser(self.application_)
            # self.application_.serviceManager_.stop('tcpdump-testing')
            test_result = parser.parse(result, True, self.application_.cwd_ + result_file + '-' + label)
            self.logInfo(inspect.currentframe().f_code.co_name, 'run ' + performance_type + ' ' + test_type + ' ' + db_type + ' rate ' + str(rate) + ' period ' + str(period) + ' test completed')
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'run ' + performance_type + ' ' + test_type + ' ' + db_type + ' test error ' + str(e))
        return test_result
