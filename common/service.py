import inspect
import logging


class Service(object):
    def __init__(self, name, manager):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.name_ = name
        self.manager_ = manager
        return

    def install(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def uninstall(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def enable(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def disable(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def start(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def stop(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def restart(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return

    def status(self):
        self.manager_.logDebug(inspect.currentframe().f_code.co_name, None, __name__)
        return
