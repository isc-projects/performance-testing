import inspect
import json
import logging.config
import os

import common.application as application


class TestApplication(application.Application):
    def __init__(self, path, tag):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        super(TestApplication, self).__init__()
        self.defaultSettingFile_ = tag + '.json'
        self.settingsFile_ = self.defaultSettingFile_
        self.defaultInternalTag_ = 'application'
        self.internalTag_ = self.defaultInternalTag_
        self.defaultApplicationTag_ = 'application'
        self.applicationTag_ = self.defaultApplicationTag_
        self.defaultLogger_ = 'root'
        self.logger_ = self.defaultLogger_
        self.defaultLoggerConfig_ = {'version': 1}
        self.loggerConfig_ = self.defaultLoggerConfig_
        self.defaultLoggerSettings_ = tag + '/application-log-default-cfg.json'
        self.loggerSettings_ = self.defaultLoggerSettings_
        self.scriptPath_ = path
        self.cwd_ = os.getcwd()
        self.done_ = False
        self.internalTag_ = tag
        self.logManager_ = None
        self.serviceManager_ = None
        self.settingsManager_ = None
        self.setupManager_ = None
        self.utilsManager_ = None
        return

    def setup(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        import common.utils_manager
        self.utilsManager_ = common.utils_manager.UtilsManager(self)
        import common.service_manager
        self.serviceManager_ = common.service_manager.ServiceManager(self)
        import common.settings_manager
        self.settingsManager_ = common.settings_manager.SettingsManager(self)
        import common.setup_manager
        self.setupManager_ = common.setup_manager.SetupManager(self)
        import common.log_manager
        self.logManager_ = common.log_manager.LogManager(self)
        return

    def load(self, config):
        self.logDebug(inspect.currentframe().f_code.co_name)
        self.applicationTag_ = self.defaultApplicationTag_
        self.logger_ = self.defaultLogger_
        self.loggerConfig_ = self.defaultLoggerConfig_
        self.loggerSettings_ = self.defaultLoggerSettings_
        applicationTag = config.get('application-tag', '')
        if applicationTag:
            self.applicationTag_ = str(applicationTag)
        logger = config.get('logger', '')
        if logger:
            self.logger_ = logger.lower()
            if self.logger_ not in ['root', 'debug', 'info', 'warning', 'error', 'critical']:
                self.logger_ = self.defaultLogger_
        loggerSettings = config.get('logger-settings', '')
        if loggerSettings:
            self.loggerSettings_ = loggerSettings
        try:
            with open(self.loggerSettings_, 'r') as settingsFile:
                self.loggerConfig_ = json.loads(settingsFile.read())
                if type(self.loggerConfig_) != type({}):
                    self.loggerConfig_ = self.defaultLoggerConfig_
                    self.logger_ = self.defaultLogger_
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not open settings file ' + self.loggerSettings_ + ' error ' + str(e))
        return True

    def apply(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        try:
            logging.config.dictConfig(self.loggerConfig_)
        except Exception as e:
            self.logError(inspect.currentframe().f_code.co_name, 'can not apply logging settings error ' + str(e))
        return

    def run(self):
        self.logDebug(inspect.currentframe().f_code.co_name)
        super(TestApplication, self).run()
        return

    def doWork(self):
        # logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        return self.work()

    def work(self):
        # logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        return True

    def handleSignals(self):
        # logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        for signal in self.signals_:
            self.signals_.remove(signal)
            raise Exception('signal ' + str(signal) + ' not handled')
        return

    def logDebug(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        if not self.logManager_:
            logging.debug(logMessage)
        else:
            self.logManager_.logDebug(logMessage)
        return

    def logInfo(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        if not self.logManager_:
            logging.info(logMessage)
        else:
            self.logManager_.logInfo(logMessage)
        return

    def logWarning(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        if not self.logManager_:
            logging.warning(logMessage)
        else:
            self.logManager_.logWarning(logMessage)
        return

    def logError(self, function, message=None, tag=None, classTag=None, logSerial=False):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        if not self.logManager_:
            logging.error(logMessage)
        else:
            self.logManager_.logError(logMessage)
        return

    def logCritical(self, function, message=None, tag=None, classTag=None, logSerial=True):
        if not message:
            message = 'called'
        if not tag:
            tag = self.applicationTag_
        else:
            tag = ''
        if not classTag:
            classTag = __name__
        logMessage = 'application [' + tag + '] class [' + classTag + '] function [' + function + '] message [' + message + ']'
        if not self.logManager_:
            logging.critical(logMessage)
        else:
            self.logManager_.logCritical(logMessage)
        return
