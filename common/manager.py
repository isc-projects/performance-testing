import inspect
import logging


class Manager(object):
    def __init__(self, name, application):
        logging.debug(__name__ + '.' + inspect.currentframe().f_code.co_name + ' called')
        self.name_ = name
        self.application_ = application
        return

    def logDebug(self, function=None, message=None, classTag=None, tag=None, logSerial=False):
        if not classTag:
            classTag = self.name_
        self.application_.logDebug(function, message, tag, classTag, logSerial)
        return

    def logInfo(self, function=None, message=None, classTag=None, tag=None, logSerial=False):
        if not classTag:
            classTag = self.name_
        self.application_.logInfo(function, message, tag, classTag, logSerial)
        return

    def logWarning(self, function=None, message=None, classTag=None, tag=None, logSerial=False):
        if not classTag:
            classTag = self.name_
        self.application_.logWarning(function, message, tag, classTag, logSerial)
        return

    def logError(self, function=None, message=None, classTag=None, tag=None, logSerial=False):
        if not classTag:
            classTag = self.name_
        self.application_.logError(function, message, tag, classTag, logSerial)
        return

    def logCritical(self, function=None, message=None, classTag=None, tag=None, logSerial=True):
        if not classTag:
            classTag = self.name_
        self.application_.logCritical(function, message, tag, classTag, logSerial)
        return
