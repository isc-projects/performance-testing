#!/usr/bin/python

import inspect
import os
import signal
import sys
import traceback

import performance_testing_application


class ExitStatus(object):
    def __init__(self):
        return

    EXIT_NO_ERROR = 0
    EXIT_ERROR = 1


def signalHandler(signalNumber, frame):
    instance.logDebug(inspect.currentframe().f_code.co_name, 'caught signal ' + str(signalNumber))
    if signalNumber == signal.SIGTERM or signalNumber == signal.SIGHUP:
        instance.stop()
    else:
        instance.registerSignal(signalNumber)
    return


def main():
    global instance
    instance = performance_testing_application.PerformanceTestingApplication(os.path.dirname(os.path.realpath(__file__)))
    instance.logDebug(inspect.currentframe().f_code.co_name)
    signal.signal(signal.SIGINT, signalHandler)
    signal.signal(signal.SIGABRT, signalHandler)
    signal.signal(signal.SIGTERM, signalHandler)
    signal.signal(signal.SIGHUP, signalHandler)
    signal.signal(signal.SIGQUIT, signalHandler)
    instance.run()
    return


if (__name__ == '__main__'):
    try:
        main()
    except Exception as e:
        instance.logError(inspect.currentframe().f_code.co_name, 'failure error ' + str(e))
        instance.logError(inspect.currentframe().f_code.co_name, traceback.format_exc())
        sys.exit(ExitStatus.EXIT_ERROR)
